# USU HR System

## Installation and Build

### Prerequisites
#### 1. git
Download and install Git from https://git-scm.com/downloads

#### 2. Node.js & NPM
Download and install Node.js from https://nodejs.org/en/download/

### Downloading project
Open command line or terminal and navigate to path, where you want to download project `cd /path/to/project`

In this folder execute following command `git clone git@bitbucket.org:CylonSK/usu-hr-demo.git`

### Installing dependencies
Navigate to downloaded folder `cd usu-hr-demo` and run `npm install`

### Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Development

### Running development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
