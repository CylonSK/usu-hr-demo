import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { APP_ROUTES } from './app.router';

import { AppComponent } from './app.component';

import { UsersModule } from './users/users.module';

import { AlertModule } from "ng2-bootstrap";
import { RestangularModule } from 'ng2-restangular';
import { GlobalRestangularConfig } from './configs/global-restangular.config';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule.forRoot(),
    APP_ROUTES,
    UsersModule,
    RestangularModule.forRoot(GlobalRestangularConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
