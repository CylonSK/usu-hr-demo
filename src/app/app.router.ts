import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {AppComponent} from "./app.component";
import {UserDashboardComponent} from "./users/user-dashboard/user-dashboard.component";
import {UserStatisticsComponent} from "./users/user-statistics/user-statistics.component";

export const APP_ROUTER: Routes = [
    { path: '', redirectTo: '/users', pathMatch: 'full'},
    { path: 'users', component: UserDashboardComponent},
    { path: 'statistics', component: UserStatisticsComponent}
];

export const APP_ROUTES: ModuleWithProviders = RouterModule.forRoot(APP_ROUTER);