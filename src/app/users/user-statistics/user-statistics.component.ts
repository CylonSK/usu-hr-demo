import { Component, OnInit } from '@angular/core';
import { UserService } from './../services/user.service';

@Component({
  selector: 'app-user-statistics',
  templateUrl: './user-statistics.component.html',
  styleUrls: ['./user-statistics.component.css']
})
export class UserStatisticsComponent implements OnInit {

  statisticsByPosition;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.statisticsByPosition().subscribe(statistics => {
      this.statisticsByPosition = statistics;
    });
  }

}
