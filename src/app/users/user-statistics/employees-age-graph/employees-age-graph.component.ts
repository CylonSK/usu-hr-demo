import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'employees-age-graph',
  templateUrl: './employees-age-graph.component.html',
  styleUrls: ['./employees-age-graph.component.css']
})
export class EmployeesAgeGraphComponent implements OnInit {

  @Input() statisticsByPosition;

  public chartData:number[] = [];
  public chartLabels:string[] = [];

  public chartType:string = "bar";

  constructor() { }

  ngOnInit() {
    this.chartData = this.parseData(this.statisticsByPosition);
    this.chartLabels = this.parseLabels(this.statisticsByPosition);
  }

  private parseData(statistics): number[] {
    var toReturn = [];

    if(!statistics) return toReturn;

    for(let i = 0; i < statistics.length; i++){
      toReturn.push(statistics[i].age);
    }


    return toReturn;
  }

  private parseLabels(statistics): string[] {
    var toReturn = [];

    if(!statistics) return toReturn;

    for(let i = 0; i < statistics.length; i++){
      toReturn.push(statistics[i].position);
    }

    return toReturn;
  }

}
