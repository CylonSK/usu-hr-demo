import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'employees-count-graph',
  templateUrl: './employees-count-graph.component.html',
  styleUrls: ['./employees-count-graph.component.css']
})
export class EmployeesCountGraphComponent implements OnInit {

  @Input() statisticsByPosition;

  public chartData:number[] = [];
  public chartLabels:string[] = [];

  public chartType:string = "doughnut";

  constructor() { }

  ngOnInit() {
    this.chartData = this.parseData(this.statisticsByPosition);
    this.chartLabels = this.parseLabels(this.statisticsByPosition);
  }

  private parseData(statistics): number[] {
    var toReturn = [];

    if(!statistics) return toReturn;

    for(let i = 0; i < statistics.length; i++){
      toReturn.push(statistics[i].count);
    }


    return toReturn;
  }

  private parseLabels(statistics): string[] {
    var toReturn = [];

    if(!statistics) return toReturn;

    for(let i = 0; i < statistics.length; i++){
      toReturn.push(statistics[i].position);
    }

    return toReturn;
  }

}
