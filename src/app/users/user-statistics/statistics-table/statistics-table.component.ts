import { Component, Input } from '@angular/core';

@Component({
  selector: 'statistics-table',
  templateUrl: './statistics-table.component.html',
  styleUrls: ['./statistics-table.component.css']
})
export class StatisticsTableComponent {

  @Input() statisticsByPosition;

  constructor() { }

}
