import { Injectable } from '@angular/core';
import { Restangular } from "ng2-restangular";
import { User } from "./../user-dashboard/user-create/user-create.component";

@Injectable()
export class UserService {

  constructor(private restangular: Restangular) { }

  getAll() {
    return this.restangular.all('employees').getList();
  }

  remove(id) {
    return this.restangular.one('employees', id).remove();
  }
  
  create(user: User) {
    return this.restangular.all('employees').post(user);
  }

  statisticsByPosition() {
    return this.restangular.all('employees').all('statistics_by_position').getList();
  }
}
