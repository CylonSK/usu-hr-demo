import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserStatisticsComponent } from './user-statistics/user-statistics.component';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { UserListComponent } from './user-dashboard/user-list/user-list.component';
import { UserCreateComponent } from './user-dashboard/user-create/user-create.component';
import { FormsModule } from '@angular/forms';
import { StatisticsTableComponent } from './user-statistics/statistics-table/statistics-table.component';
import { EmployeesCountGraphComponent } from './user-statistics/employees-count-graph/employees-count-graph.component';
import { ChartsModule } from 'ng2-charts';
import { EmployeesAgeGraphComponent } from './user-statistics/employees-age-graph/employees-age-graph.component';
import { UserService } from "./services/user.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule
  ],
  providers: [UserService],
  declarations: [UserStatisticsComponent, UserDashboardComponent, UserListComponent, UserCreateComponent, StatisticsTableComponent, EmployeesCountGraphComponent, EmployeesAgeGraphComponent]
})
export class UsersModule { }
