import { Component, Input } from '@angular/core';
import { UserService } from "./../../services/user.service";

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent {

  @Input() users;

  constructor(private userService: UserService) { }

  deleteUser(user): void {
    this.userService.remove(user.id)._subscribe(() => {
      let index = this.users.indexOf(user);
      if(index >= 0 ){
        this.users.splice(index, 1);
      }
    })
  }

}
