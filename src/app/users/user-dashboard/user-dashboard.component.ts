import { Component, OnInit } from '@angular/core';
import { UserService } from "./../services/user.service";

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {

  users;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAll()._subscribe(users => {
      this.users = users;
    });
  }

  addUser(user) {
    this.users.push(user);
  }
}
