import { Component, Output, EventEmitter } from '@angular/core';
import { UserService } from "./../../services/user.service";

@Component({
  selector: 'user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent {

  @Output() userCreated = new EventEmitter();

  user = new User();

  positions = ['BACKEND_DEVELOPER', 'FRONTEND_DEVELOPER', 'PRODUCT_OWNER', 'SCRUM_MASTER'];

  constructor(private userService: UserService) { }

  onSubmit(){
    this.userService.create(this.user).subscribe(user => {
      this.userCreated.emit(user);
    });

    this.user = new User();
  }
}


export class User {
  constructor(
      public name?: string,
      public age?: number,
      public position?: string
  ) {  }
}